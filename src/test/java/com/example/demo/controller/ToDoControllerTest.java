package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles(profiles = "test")
public class ToDoControllerTest extends AbstractControllerTest {

    @Autowired
    private ToDoRepository toDoRepository;

    @BeforeEach
    void clearBefore() {
        toDoRepository.deleteAll();
    }
    @AfterEach
    void clearAfter() {
        toDoRepository.deleteAll();
    }

    @Test
    void handleException() {
    }

    @Test
    void shouldReturnAllTodos() throws Exception {
        ToDoSaveRequest todoSaveRequest1 = new ToDoSaveRequest();
        todoSaveRequest1.text = "text_1";

        ToDoSaveRequest todoSaveRequest2 = new ToDoSaveRequest();
        todoSaveRequest2.text = "text_2";

        ToDoResponse toDoResponse1 = createAndAssert(todoSaveRequest1);
        ToDoResponse toDoResponse2 = createAndAssert(todoSaveRequest2);

        mockMvc.perform(get("/todos"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].id").exists())
                .andExpect(jsonPath("$[*].text", containsInAnyOrder(toDoResponse1.text, toDoResponse2.text)))
                .andExpect(jsonPath("$[*].completedAt").exists());
    }

    @Test
    void shouldSaveToDo() throws Exception {
        ToDoSaveRequest todoSaveRequest = new ToDoSaveRequest();
        todoSaveRequest.text = "text";

        ToDoResponse toDoResponse = createAndAssert(todoSaveRequest);
        assertThat(toDoResponse.id, notNullValue());
        assertThat(toDoResponse.text, equalTo(todoSaveRequest.text));

    }

    private ToDoResponse createAndAssert(ToDoSaveRequest todoSaveRequest) throws Exception {
        String json = objectMapper.writeValueAsString(todoSaveRequest);

        MvcResult mvcResult = mockMvc
                .perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isOk()) // isCreated() should be here
                .andReturn();

        ToDoEntity toDoEntityResult = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ToDoEntity.class);

        return ToDoEntityToResponseMapper.map(toDoEntityResult);
    }

    @Test
    void shouldReturnToDoByExistedId() throws Exception {
        ToDoSaveRequest todoSaveRequest = new ToDoSaveRequest();
        todoSaveRequest.text = "text_test";

        ToDoResponse response = createAndAssert(todoSaveRequest);

        mockMvc.perform(get("/todos/" + response.id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(response.id))
                .andExpect(jsonPath("$.text").value(response.text));
    }

    @Test
    void shouldNotReturnToDoByNotExistedId() throws Exception {
        ToDoSaveRequest todoSaveRequest = new ToDoSaveRequest();
        todoSaveRequest.text = "text_test";

        createAndAssert(todoSaveRequest);

        long unknownId = 2L;
        mockMvc.perform(get("/todos/" + unknownId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", containsString("Can not find todo with id " + unknownId)));
    }

    @Test
    void toDoEntityShouldBeDeleted() throws Exception {
        ToDoSaveRequest todoSaveRequest = new ToDoSaveRequest();
        todoSaveRequest.text = "text_test";

        ToDoResponse response = createAndAssert(todoSaveRequest);

        mockMvc.perform(delete("/todos/" + response.id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").doesNotExist());
    }
}
